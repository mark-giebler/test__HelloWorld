helloworld : helloworld.c
	gcc -o helloworld helloworld.c

clean:
	rm -f helloworld

test:
	./helloworld | grep World
