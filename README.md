<!-- Hint: use reText markdown editor for WYSIWIG review -->

# Hello World test repository

[![Release](https://gitlab.com/mark-giebler/test__HelloWorld/-/badges/release.svg)](https://gitlab.com/mark-giebler/test__HelloWorld/-/releases)
[![Pipeline Status](https://gitlab.com/mark-giebler/test__HelloWorld/badges/master/pipeline.svg)](https://gitlab.com/mark-giebler/test__HelloWorld/-/pipelines)

This repository is for testing Gitlab CI and automatic release generation.

Releases for this project can be found on the [Releases page](./../../releases).

The release descriptions on the Releases page are automatically extracted from the CHANGELOG file by this project's CI.

The CI automation yaml that creates the releases for this project is in the [.gitlab-ci.yml](.gitlab-ci.yml) file.

Author: Mark Giebler

## References

Some example Gitlab yaml scripts for various languages are [here](https://docs.gitlab.com/ee/ci/examples/).

Below are various *How To* references that were helpful in making this possible.

### Gitlab CI:

Good overall info on *how to* make discrete jobs and how to publish artifacts in this
[YouTube video.](https://youtu.be/EuwLdbCu3DE?t=743).

[Slides](https://petejohanson.gitlab.io/nerdsummit-2019-gitlab-ci-cd/#/) for the video.

Info on making [packages](https://docs.gitlab.com/ee/user/project/releases/#use-a-generic-package-for-attaching-binaries)
for attaching binary artifacts. Only available on EE server version, not CE.

Other useful information:

- To prevent CI from running on commits, put the string "**[skip ci]**" at the beginning of
the commit message.

### Gitlab Release CLI tool

**Making a release in gitlab:**

Command line release-cli is in a docker container: **registry.gitlab.com/gitlab-org/release-cli**

The container can be used in the YAML file. **We use it in this project.**

Using it in .gitlab-ci.yml:

- Part 1: example with a manual release pipeline [here](https://www.youtube.com/watch?v=nCtA0iomMpQ)

- Part 2: example release made when tag pushed [here](https://www.youtube.com/watch?v=YtaqZMwx7sA&t=0s)

**Other links:**

		https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs#gitlab-release-command-line-tool

		https://gitlab.com/gitlab-org/release-cli

Example project used in Part 1 & 2 youtube videos [here](https://gitlab.com/jaime/hello).

YAML release [doc](https://docs.gitlab.com/ee/ci/yaml/#release)

Info on using "filepath" parameter in the release CLI tool to create permanent and consistent URL links
to releases,
see [here for details](https://docs.gitlab.com/ee/user/project/releases/#permanent-links-to-release-assets).

More info on making release and info on including binary outputs on [stackOverFlow](https://stackoverflow.com/a/29521646).

Example using older non-command line version (REST API) is [here](https://www.garybell.co.uk/creating-a-release-with-gitlab-ci-and-composer/).

### CHANGELOG

Automating the creation of the CHANGELOG.md file when creating a release would be nice.

In this section I gather some information related to various ways to do this in Gitlab.

Documentation on a GO based ["release" command](https://juhani.gitlab.io/go-semrel-gitlab/) on Gitlab.io.
Available as a Docker container, are as executable Linux binary.  This seems the most promising.
It does require using trigger words in the commit comments.  Not certain if they are configurable.

Example using "release" command to [update CHANGELOG.]((https://www.infralovers.com/en/articles/2020/02/11/gitlab-changelog-automation/)

Gitlab's [CHANGELOG information](https://docs.gitlab.com/ee/development/changelog.html) seems to indicate that CHANGELOG updating is built in?

Gitlab's documentation on git commit message [meta tags for automating changelog.](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/development/changelog.md)

Maybe this is an Example that [automates CHANGELOG updating.](https://github.com/gitlabhq/gitlab-runner/blob/master/.gitlab/ci/docs.gitlab-ci.yml).

And the examples [CHANGELOG config](https://github.com/gitlabhq/gitlab-runner/blob/master/.gitlab/changelog.yml).

### Gitlab Templates:

[Gitlab's template examples.](https://docs.gitlab.com/ee/user/project/description_templates.html#description-template-example)
